# PBSperformance

Does basic model evaluation by cross validation (jackknife and retrospective) - and calculation of performance metrics along with model ranking.

### Install
 To install this package directly from gitlab, use:

```
install.packages("remotes") # Install the remotes package
url.use <- "https://gitlab.com/MichaelFolkes/PBSperformance.git" 
remotes::install_git(url = url.use)
``` 
